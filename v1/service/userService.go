package service

import (
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/investio/backend/user-api/db"
	"gitlab.com/investio/backend/user-api/v1/model"
)

type UserService struct{}

func NewUserService() *UserService {
	return &UserService{}
}

func (s *UserService) Create(newUser *model.User) (err error) {
	log.Info("New User ", newUser)
	if err = db.UserDB.Create(newUser).Error; err != nil {
		return
	}

	ud := model.UserData{
		UserID: newUser.ID,
	}

	err = db.UserDB.Create(&ud).Error
	return
}

func (s *UserService) GetByUsername(user *model.User, username string) (err error) {
	err = db.UserDB.Where("name = ?", username).First(&user).Error
	return
}

func (s *UserService) GetByUserID(user *model.User, userID uint) (err error) {
	err = db.UserDB.First(&user, userID).Error
	return
}

func (s *UserService) GetUserData(userData *model.UserData, userID uint) (err error) {
	if err = db.UserDB.Where("user_id = ?", userID).First(&userData).Error; err != nil {
		return
	}
	return
}

func (s *UserService) GetRiskScore(userID uint) (score uint8, updatedAt time.Time, err error) {
	var userData model.UserData
	if err = db.UserDB.Where("user_id = ?", userID).First(&userData).Error; err != nil {
		return
	}
	score = userData.RiskScore
	updatedAt = userData.RiskUpdatedAt
	return
}

func (s *UserService) SetRiskScore(userID uint, score uint8) (err error) {
	var userData model.UserData
	if err = db.UserDB.Where("user_id = ?", userID).First(&userData).Error; err != nil {
		return
	}
	userData.RiskScore = score
	userData.RiskUpdatedAt = time.Now()

	err = db.UserDB.Save(&userData).Error
	return
}
