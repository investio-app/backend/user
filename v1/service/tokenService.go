package service

import (
	"crypto/ed25519"
	"os"
	"time"

	log "github.com/sirupsen/logrus"

	"github.com/twinj/uuid"
	"gitlab.com/investio/backend/user-api/v1/schema"
	jose "gopkg.in/square/go-jose.v2"
	"gopkg.in/square/go-jose.v2/jwt"
)

type TokenService struct {
	builder        jwt.Builder
	atkTTL, rtkTTL time.Duration
}

func NewTokenService(atk, rtk string) *TokenService {
	atkTtl, err := time.ParseDuration(atk)
	if err != nil {
		log.Panic("Token - Parse ATK faled", err)
	}
	rtkTtl, err := time.ParseDuration(rtk)
	if err != nil {
		log.Panic("Token - Parse RTK faled", err)
	}

	log.Info("Set ATK = ", atkTtl.String(), " RTK = ", rtkTtl.String())
	return &TokenService{
		atkTTL: atkTtl,
		rtkTTL: rtkTtl,
	}
}

func (s *TokenService) CreateTokens(userID uint) (tokens schema.Tokens, err error) {
	accessJWT, accessClaims, err := s.IssueToken(userID, schema.AccessTokenType)
	if err != nil {
		return
	}
	refreshJWT, refreshClaims, err := s.IssueToken(userID, schema.RefreshTokenType)

	tokens = schema.Tokens{
		AccessToken:  accessJWT,
		AcsExpires:   accessClaims.Expiry.Time().Unix(),
		RefreshToken: refreshJWT,
		RefExpires:   refreshClaims.Expiry.Time().Unix(),
	}
	return
}

func (s *TokenService) IssueToken(userID uint, tokenType schema.TokenType) (JwtStr string, tokenClaims schema.TokenClaims, err error) {
	// ACS_TTL := time.Minute * time.Duration(s.ATK_TTL)
	// REF_TTL := time.Hour * time.Duration(s.RTK_TTL)

	if s.builder == nil {
		var (
			privateKey ed25519.PrivateKey
			seed       []byte
			rsaSigner  jose.Signer
		)

		// Read seed
		seed, err = os.ReadFile("./keys/token.key")
		if err != nil {
			// Create a new key
			privateKey, _, err = s.genKeyAndWriteSeed()
			if err != nil {
				panic(err)
			}
		} else {
			// Create the key from seed
			privateKey = ed25519.NewKeyFromSeed(seed)
		}

		// create Square.jose signing key
		key := jose.SigningKey{Algorithm: jose.EdDSA, Key: privateKey}
		// create a Square.jose RSA signer, used to sign the JWT
		var signerOpts = jose.SignerOptions{}
		signerOpts.WithType("JWT")
		rsaSigner, err = jose.NewSigner(key, &signerOpts)
		if err != nil {
			log.Warn("failed to create signer: ", err)
			return
		}
		// create an instance of Builder that uses the rsa signer
		s.builder = jwt.Signed(rsaSigner)
	}

	issueTime := jwt.NewNumericDate(time.Now())
	var (
		isRefreshToken bool
		expTime        time.Time
	)

	if tokenType == schema.AccessTokenType {
		expTime = time.Now().Add(s.atkTTL)

		// create a Claim of the Access Token

	} else if tokenType == schema.RefreshTokenType {
		expTime = time.Now().Add(s.rtkTTL)
		// create a Claim of the Refresh Token
		isRefreshToken = true
	}

	log.Debug("Issued token exp at ", expTime.Local().String(), " isRefresh=", isRefreshToken)

	tokenClaims = schema.TokenClaims{
		Claims: &jwt.Claims{
			Issuer:   "investio-api:user",
			Subject:  "acs:you@inv.me",
			ID:       uuid.NewV4().String(),
			Audience: jwt.Audience{"investio.dewkul.me", "investio.netlify.app"},
			IssuedAt: issueTime,
			Expiry:   jwt.NewNumericDate(expTime),
		},
		UserID:       userID,
		IsAuthorized: true,
		IsRefresh:    isRefreshToken,
	}

	// add claims to the Builder
	s.builder = s.builder.Claims(tokenClaims)

	// validate all ok, sign with the Ed25519 key, and return a compact JWT
	JwtStr, err = s.builder.CompactSerialize()
	if err != nil {
		log.Warn("failed to create JWT: ", err)
	}
	return
}

func (s *TokenService) genKeyAndWriteSeed() (privKey ed25519.PrivateKey, pubKey ed25519.PublicKey, err error) {
	pubKey, privKey, err = ed25519.GenerateKey(nil)
	if err != nil {
		return
	}
	seed := privKey.Seed()
	err = os.WriteFile("./keys/token.key", seed, 0600)
	return
}
