package main

import (
	"context"
	"os"
	"strings"

	"github.com/sirupsen/logrus"

	_ "time/tzdata"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"gitlab.com/investio/backend/user-api/db"
	"gitlab.com/investio/backend/user-api/v1/controller"
	"gitlab.com/investio/backend/user-api/v1/service"
)

var (
	log = logrus.New()
)

func getVersion(ctx *gin.Context) {
	ctx.JSON(200, gin.H{
		"version": "1.2.2",
	})
}

func setCors() (corsConfig cors.Config) {
	corsConfig = cors.DefaultConfig()
	origins := os.Getenv("ALLOW_ORIGINS")
	oriList := strings.Split(origins, ",")
	if oriList[0] == "" {
		oriList = []string{"*"}
	}
	corsConfig.AllowOrigins = oriList

	// corsConfig.AllowMethods = []string{"PUT"}
	corsConfig.AllowHeaders = []string{"Authorization", "content-type"}
	// To be able to send tokens to the server.
	corsConfig.AllowCredentials = true

	// OPTIONS method for VueJS
	corsConfig.AddAllowMethods("OPTIONS")
	return
}

func main() {
	if os.Getenv("GIN_MODE") != "release" {
		err := godotenv.Load()
		if err != nil {
			log.Warn("Main: Fail to load .env file")
		}
	}

	// logLevel := os.Getenv("LOG_LEVEL")
	log.SetLevel(logrus.DebugLevel)

	aTkTTL := os.Getenv("ATK_TTL")
	if aTkTTL == "" {
		log.Warn("Main: using default aTk = 45 mins")
		aTkTTL = "45m"
	}

	rTkTTL := os.Getenv("RTK_TTL")
	if rTkTTL == "" {
		log.Warn("Main: using default rTk = 18 hrs")
		rTkTTL = "18h"
	}

	token := service.NewTokenService(aTkTTL, rTkTTL)
	auth := service.NewAuthService()
	redis := service.NewRedisService(context.Background())
	user := service.NewUserService()

	userController := controller.NewUserController(token, auth, redis, user)

	if err := db.SetupDB(); err != nil {
		log.Panic(err)
	}

	if err := redis.TestConnection(); err != nil {
		log.Panic(err)
	}

	// r := gin.New()
	// r.Use(ginlogrus.Logger(log), gin.Recovery())

	r := gin.Default()
	r.Use(cors.New(setCors()))

	v1 := r.Group("/v1")
	{
		v1.POST("/login", userController.Login)
		v1.POST("/logout", userController.LogOut)
		v1.POST("/refresh", userController.Refresh)
		v1.POST("/create", userController.RegisterUser)
		v1.GET("/data", userController.GetUserData)
		v1.GET("/data/risk", userController.GetRiskScore)
		v1.POST("/data/risk", userController.UpdateRiskScore)
		v1.GET("/ver", getVersion)
		// data := r.Group("/data")
		// {
		// 	data.GET("/risk", userController.GetRiskScore)
		// 	data.POST("/risk", userController.UpdateRiskScore)
		// }
	}
	port := os.Getenv("API_PORT")
	if port == "" {
		port = "5005"
	}
	log.Panic(r.Run(":" + port))
}
